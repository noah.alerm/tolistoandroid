package com.example.tolistoandroid.data.models

import com.google.gson.annotations.SerializedName

data class Llista(@SerializedName("listId") var id: Long?, var name: String, var tasks: ArrayList<Task>)
