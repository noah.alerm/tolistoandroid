package com.example.tolistoandroid.ui.views


import android.annotation.SuppressLint
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Rect
import android.graphics.Typeface
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.transition.TransitionInflater
import android.util.Log
import android.util.TypedValue
import android.view.Gravity
import android.view.View
import android.view.animation.Animation
import android.view.animation.AnimationUtils.loadAnimation
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.content.res.AppCompatResources.getDrawable
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.Navigation
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.example.tolistoandroid.R
import com.example.tolistoandroid.data.models.Llista
import com.example.tolistoandroid.ui.adapters.ListsAdapter
import com.example.tolistoandroid.ui.viewModel.ViewModel
import com.example.tolistoandroid.utils.PopupMenuMethods
import com.google.android.material.floatingactionbutton.FloatingActionButton

import it.xabaras.android.recyclerview.swipedecorator.RecyclerViewSwipeDecorator

class ListsFragment : Fragment(R.layout.fragment_lists) {
    //ATTRIBUTES
    private lateinit var recyclerView: RecyclerView
    private lateinit var addButton: FloatingActionButton
    private lateinit var goBackButton: ImageView
    private lateinit var menuButton: ImageView
    private lateinit var searchButton: ImageView

    //ANIMATION ITEMS
    private var fabOpen: Animation? = null
    private var fabClose: Animation? = null
    private var rotateForward: Animation? = null
    private var rotateBackward: Animation? = null
    private var isOpen = false

    //view model
    private val viewModel: ViewModel by activityViewModels()

    //refresh
    private lateinit var swipeRefresh: SwipeRefreshLayout

    //ON VIEW CREATED
    @SuppressLint("SetTextI18n", "CutPasteId", "Recycle", "ObjectAnimatorBinding")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        requireContext().theme.applyStyle(R.style.Theme_ToListOAndroid, true)

        //TRANSITION
        if (!viewModel.comesFromSelf) {
            val inflater = TransitionInflater.from(requireContext())
            enterTransition = inflater.inflateTransition(R.transition.slide_right).setDuration(800)
        }
        viewModel.comesFromSelf = false

        //LISTS UPDATE
        viewModel.getListsFromAPI()

        //IDs
        recyclerView = view.findViewById(R.id.recycler_view_lists)
        addButton = view.findViewById(R.id.add_button)
        goBackButton = view.findViewById(R.id.go_back_button_list)
        menuButton = view.findViewById(R.id.menu_button)
        searchButton = view.findViewById(R.id.search_icon_lists)
        swipeRefresh = view.findViewById(R.id.refresh)

        //REFRESH
        swipeRefresh.setOnRefreshListener {
            viewModel.getListsFromAPI()

            //NAVIGATION
            Handler(Looper.getMainLooper()).postDelayed({
                findNavController().navigate(R.id.action_lists_self)
                viewModel.comesFromSelf = true
            }, 500)
        }

        //RECYCLER VIEW
        recyclerView.layoutManager = LinearLayoutManager(requireContext())

        if (viewModel.isFirstTime) {
            viewModel.isFirstTime = false
            Handler(Looper.getMainLooper()).postDelayed({
                recyclerView.adapter = ListsAdapter(viewModel.getLists())
            }, 500)
        }
        else
            recyclerView.adapter = ListsAdapter(viewModel.getLists())

        //ANIMATION IDs
        fabOpen = loadAnimation(requireContext(), R.anim.fab_open)
        fabClose = loadAnimation(requireContext(), R.anim.fab_close)
        rotateForward = loadAnimation(requireContext(), R.anim.rotate_forward)
        rotateBackward = loadAnimation(requireContext(), R.anim.rotate_backward)

        //RECYCLER VIEW MANAGEMENT
        ItemTouchHelper(object : ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT) {
            //MOVE
            override fun onMove(recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder, target:
            RecyclerView.ViewHolder): Boolean = false

            //SWIPE
            override fun onSwiped(h: RecyclerView.ViewHolder, dir: Int) {
                val position = h.bindingAdapterPosition
                val item = viewModel.getLists()[position]

                //CUSTOM TITLE
                val title = TextView(requireContext())
                title.text = "DELETE"
                title.setTextSize(TypedValue.COMPLEX_UNIT_SP, 30F)
                title.gravity = Gravity.CENTER
                title.setTextColor(ContextCompat.getColor(requireContext(), R.color.text))
                title.typeface = Typeface.DEFAULT_BOLD
                title.background = getDrawable(requireContext(), R.drawable.add_list_dialog_title_bg)
                title.setPadding(0, 30, 0, 30)

                //CUSTOM DIALOG
                val builder = AlertDialog.Builder(requireContext(), R.style.AddListDialogTheme)
                builder.setMessage("Are you sure you want to delete this list and all of its items?")
                builder.setCustomTitle(title)
                builder.setNegativeButton("CANCEL") { _, _ ->
                    viewModel.comesFromSelf = true
                    Navigation.findNavController(view).navigate(R.id.action_lists_self)
                }
                builder.setPositiveButton("DELETE LIST") { _, _ ->
                    //DELETE
                    viewModel.deleteListFromAPI(item.id!!)

                    //Local update
                    viewModel.getLists().removeAt(position)
                    Log.d("DELETE", viewModel.getLists().toString())

                    //Recycler view update
                    (recyclerView.adapter as ListsAdapter).notifyItemRemoved(position)
                }
                builder.setCancelable(false)
                val dialog = builder.create()
                dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT)) //Set to Transparent to only see the custom bg.
                dialog.show()
            }

            //CHILD DRAW
            override fun onChildDraw(c: Canvas, recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder,
                dX: Float, dY: Float, actionState: Int, isCurrentlyActive: Boolean) {
                super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive)

                //RED BACKGROUND
                var bg = ContextCompat.getDrawable(requireContext(), R.drawable.swipe_to_delete_bg)
                bg?.bounds = Rect(
                    viewHolder.itemView.right + dX.toInt() - 100, viewHolder.itemView.top,
                    viewHolder.itemView.right, viewHolder.itemView.bottom
                )
                bg?.draw(c)

                //NORMAL BACKGROUND
                //We need to reset the background due to the view's rounded corners.
                if (dX == 0F) {
                    bg = ContextCompat.getDrawable(requireContext(), R.color.background)
                    bg?.bounds = Rect(
                        viewHolder.itemView.right + dX.toInt() - 100, viewHolder.itemView.top,
                        viewHolder.itemView.right, viewHolder.itemView.bottom
                    )
                    bg?.draw(c)
                }


                //SWIPE DECORATOR (see Gradle Implementation)
                RecyclerViewSwipeDecorator.Builder(
                    requireContext(), c, recyclerView, viewHolder, dX,
                    dY, actionState, isCurrentlyActive
                )
                    .addActionIcon(R.drawable.trash_can)
                    .create().decorate()
            }
        }).attachToRecyclerView(recyclerView)

        //ON CLICK
        //go back button
        goBackButton.setOnClickListener {
            if (!viewModel.comesFromSearch)
                Navigation.findNavController(view).navigate(R.id.action_lists_to_menu)
            else
                recyclerView.adapter = ListsAdapter(viewModel.getLists())
            viewModel.comesFromSearch = false
        }

        //menu button
        menuButton.setOnClickListener { v: View ->
            PopupMenuMethods.showMenu(v, R.menu.popup_menu, requireContext(), false)
        }

        //add button
        addButton.setOnClickListener {
            //CUSTOM TITLE
            val title = TextView(requireContext())
            title.text = "NEW LIST"
            title.setTextSize(TypedValue.COMPLEX_UNIT_SP, 30F)
            title.gravity = Gravity.CENTER
            title.setTextColor(ContextCompat.getColor(requireContext(), R.color.text))
            title.typeface = Typeface.DEFAULT_BOLD
            title.background = getDrawable(requireContext(), R.drawable.add_list_dialog_title_bg)
            title.setPadding(0, 30, 0, 30)

            //CUSTOM DIALOG
            val builder = AlertDialog.Builder(requireContext(), R.style.AddListDialogTheme)
            val viewDialog = layoutInflater.inflate(R.layout.custom_add_list_dialog, null)
            builder.setView(viewDialog)
            builder.setCustomTitle(title)
            builder.setNegativeButton("CANCEL", null)
            builder.setPositiveButton("ADD") { _, _ ->
                //ATTRIBUTES
                val textInput: EditText = viewDialog.findViewById(R.id.add_list_name)
                val lists = viewModel.getLists()

                //ADDITION
                if (textInput.text.toString() != "") {
                    //NEW LIST
                    val newList = Llista(null, textInput.text.toString(), arrayListOf())

                    //Local update
                    lists.add(newList)

                    //POST
                    viewModel.addListToAPI(newList)

                    Log.d("ADD", viewModel.getLists().toString())

                    //Recycler view update
                    (recyclerView.adapter as ListsAdapter).notifyItemInserted(lists.size)

                    //NAVIGATION
                    findNavController().navigate(R.id.action_lists_self)
                    viewModel.comesFromSelf = true

                }
            }
            builder.setCancelable(false)
            val dialog = builder.create()
            dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT)) //Set to Transparent to only see the custom bg.
            dialog.show()

            //ANIMATION
            animateFab()
        }

        //search button
        searchButton.setOnClickListener {
            //CUSTOM TITLE
            val title = TextView(requireContext())
            title.text = "SEARCH LISTS"
            title.setTextSize(TypedValue.COMPLEX_UNIT_SP, 30F)
            title.gravity = Gravity.CENTER
            title.setTextColor(ContextCompat.getColor(requireContext(), R.color.text))
            title.typeface = Typeface.DEFAULT_BOLD
            title.background = getDrawable(requireContext(), R.drawable.add_list_dialog_title_bg)
            title.setPadding(0, 30, 0, 30)

            //CUSTOM DIALOG
            val builder = AlertDialog.Builder(requireContext(), R.style.SearchDialog)
            val viewDialog = layoutInflater.inflate(R.layout.custom_search_dialog, null)
            builder.setView(viewDialog)
            builder.setCustomTitle(title)
            builder.setNegativeButton("CANCEL", null)
            builder.setPositiveButton("SEARCH") { _, _ ->
                //ATTRIBUTES
                val textInput: EditText = viewDialog.findViewById(R.id.search_input)

                //SEARCH
                if (textInput.text.toString() != "") {
                    getFilteredList(textInput.text.toString())
                }
            }
            builder.setCancelable(false)
            val dialog = builder.create()
            dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT)) //Set to Transparent to only see the custom bg.
            dialog.show()
        }
    }

    //METHODS
    /**
     * This method is used to add an animation to the add button.
     */
    private fun animateFab() {
        isOpen = if (isOpen) {
            addButton.startAnimation(rotateForward)
            false
        } else {
            addButton.startAnimation(rotateForward)
            true
        }
    }

    /**
     * This method is used to filter the recycler view of the lists.
     * @param searchedText Characters used to filter the lists.
     */
    private fun getFilteredList(searchedText: String) {
        recyclerView.adapter = ListsAdapter(viewModel.getLists().filter { it.name.contains(searchedText) })
        viewModel.comesFromSearch = true
    }
}
