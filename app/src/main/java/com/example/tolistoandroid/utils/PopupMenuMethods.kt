package com.example.tolistoandroid.utils

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Color
import android.graphics.Typeface
import android.graphics.drawable.ColorDrawable
import android.util.TypedValue
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.animation.AnimationUtils
import android.widget.PopupMenu
import android.widget.TextView
import androidx.annotation.MenuRes
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.content.res.AppCompatResources
import androidx.core.content.ContextCompat
import androidx.navigation.Navigation
import com.example.tolistoandroid.R

object PopupMenuMethods {
    //MAIN METHOD
    /**
     * This method is used to show the popup menu.
     */
    @SuppressLint("InflateParams")
    fun showMenu(v: View, @MenuRes menuRes: Int, context: Context, fromTasks: Boolean) {
        val popup = PopupMenu(context, v)
        popup.menuInflater.inflate(menuRes, popup.menu)
        popup.setOnMenuItemClickListener { item ->
            when (item.itemId) {
                R.id.option_1
                -> listsNavigation(v, fromTasks)
                R.id.option_2
                -> dialogWho(context)
                R.id.option_3
                -> dialogHow(context)
            }
            true
        }
        //Show the popup menu.
        popup.show()
    }

    //MENU OPTIONS
    /**
     * This method manages the navigation to the lists fragment.
     */
    private fun listsNavigation(v: View, fromTasks: Boolean) {
        if (fromTasks)
            Navigation.findNavController(v).navigate(R.id.action_tasks_to_lists)
    }

    /**
     * This method is used to display the 'About US' dialog.
     */
    @SuppressLint("SetTextI18n")
    fun dialogHow(context: Context){
        //CUSTOM TITLE
        val title = TextView(context)
        title.text = "HELP"
        title.setTextSize(TypedValue.COMPLEX_UNIT_SP, 30F)
        title.gravity = Gravity.CENTER
        title.setTextColor(ContextCompat.getColor(context, R.color.text))
        title.typeface = Typeface.DEFAULT_BOLD
        title.background = AppCompatResources.getDrawable(context,
            R.drawable.how_who_dialog_title_bg
        )
        title.setPadding(0, 30, 0, 30)

        //CUSTOM DIALOG
        val builder = AlertDialog.Builder(context, R.style.HelpDialogWhoTheme)
        val viewDialog = LayoutInflater.from(context).inflate(R.layout.custom_help_dialog, null)
        builder.setView(viewDialog)
        builder.setCustomTitle(title)
        builder.setPositiveButton("OK", null)
        builder.setCancelable(false)
        val dialog = builder.create()
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT)) //Set to Transparent to only see the custom bg.
        dialog.show()

        //ANIMATION
        viewDialog.startAnimation(AnimationUtils.loadAnimation(context, R.anim.slide_up))
    }

    /**
     * This method is used to display the 'Help' dialog.
     */
    @SuppressLint("SetTextI18n")
    fun dialogWho(context: Context){
        //CUSTOM TITLE
        val title = TextView(context)
        title.text = "ABOUT US"
        title.setTextSize(TypedValue.COMPLEX_UNIT_SP, 30F)
        title.gravity = Gravity.CENTER
        title.setTextColor(ContextCompat.getColor(context, R.color.text))
        title.typeface = Typeface.DEFAULT_BOLD
        title.background = AppCompatResources.getDrawable(context,
            R.drawable.how_who_dialog_title_bg
        )
        title.setPadding(0, 30, 0, 30)

        //CUSTOM DIALOG
        val builder = AlertDialog.Builder(context, R.style.HelpDialogWhoTheme)
        val viewDialog = LayoutInflater.from(context).inflate(R.layout.custom_who_dialog, null)
        builder.setView(viewDialog)
        builder.setCustomTitle(title)
        builder.setPositiveButton("OK", null)
        builder.setCancelable(false)
        val dialog = builder.create()
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT)) //Set to Transparent to only see the custom bg.
        dialog.show()

        //ANIMATION
        viewDialog.startAnimation(AnimationUtils.loadAnimation(context, R.anim.slide_up))
    }
}
